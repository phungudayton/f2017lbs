//file HelloAspect.java
public aspect HelloAspect{
    void before(): call(void Hello.greeting()){
          System.out.print("AOP test: ");    
    }
}
